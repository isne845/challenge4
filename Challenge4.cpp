#include<bits/stdc++.h>
#include<conio.h>
using namespace std;

//movement
int movex(int x,int movex){
	if(x==9||x==6||x==3)return movex+1;
	else if(x==7||x==4||x==1)return movex-1;
	else return movex;
}

int movey(int x,int movey){
	if(x==7||x==8||x==9)return movey-1;
	else if(x==1||x==2||x==3)return movey+1;
	else return movey;
}

//make display grid
void makegrid(int posx,int posy,int Eposx[],int Eposy[],int Enum){
	for(int y=1;y<=10;y++){
		cout<<setw(10);
		for(int x=1;x<=10;x++){
			
			cout<<"[";

			//position of player every turn
			if(x==posx&&y==posy)cout<<"S";
			
			//fixed position of reindeer
			else if(x==10&&y==10)cout<<"R";
			
			//position of Evil
			for(int i=0;i<Enum;i++){
				if(x==Eposx[i]&&y==Eposy[i])cout<<"E";
			}
			
			cout<<"]";
		}
		cout<<endl;
	}
}

//main function
int main(){
	srand (time(NULL));
	//player position
	int posx=1,posy=1,move;
	//gameEnd 
	int gameover = 0,pass=0,level=1;
	//mob
	int Enum;
	
	
	do{
		//current level of game
		Enum = level*3;
		int Eposx[Enum],Eposy[Enum];
		
		//Start Position of Evil
		for(int i=0;i<Enum;i++){
			Eposx[i]=rand()%10+1;
			Eposy[i]=rand()%10+1;
			//check if Evil spawn at start
			if(Eposx[i]==1&&Eposy[i]==1){
				Eposx[i]++;
			}
			if(Eposx[i]==10&&Eposy[i]==10){
				Eposx[i]--;
			}
		}
		
		do{
			//game start
			cout<<"------Welcome to Santa simulation game------"<<endl<<endl;
			cout<<"---------------- Now Level "<<level<<"----------------"<<endl<<endl;
			makegrid(posx,posy,Eposx,Eposy,Enum);
			cout<<endl<<"--------------------------------------------"<<endl;
			cout<<setw(28)<<"Character controll"<<endl<<endl;
			cout<<"|(7)   Top Left  |(8)   Top  |(9)  Top Right   |"<<endl;
			cout<<"|(4)     Left    |(5)  Hold  |(6)    Right     |"<<endl;
			cout<<"|(1) Bottom Left |(2) Bottom |(3) Bottom Right |"<<endl;
			cout<<endl<<"--------------------------------------------"<<endl;
			
			if(posx==10&&posy==10){
				pass=1;
			}
			for(int i=0;i<Enum;i++){
				if(Eposx[i]==posx&&Eposy[i]==posy){
					gameover=1;
				}
			}
			
			
			if(pass==1){
				cout<<"Santa reached the reindeer! Mission Complete!"<<endl;
				cout<<"Press Any button to go to next stage";
				getch();
			}
			else if(gameover==1){
				cout<<"Mission fail... Try again!"<<endl;
				cout<<"Press Any button to try again";
				getch();
			}
			else{
			cout<<"Input number to make a move : ";
			cin>>move;
			
			//player move
			posx = movex(move,posx);
			posy = movey(move,posy);
			
			//Evil moving
			for(int i=0;i<Enum;i++){
				int x;
				if(Eposx[i]==1){
					do{
						x=rand()%9+1;
					}while(x==7||x==1||x==4);
				}
				else if(Eposx[i]==10){
					do{
						x=rand()%9+1;
					}while(x==9||x==6||x==3);
				}
				else x=rand()%9+1;
				Eposx[i] = movex(x,Eposx[i]);	
			}
			for(int i=0;i<Enum;i++){
				int x;
				if(Eposy[i]==10){
					do{
						x=rand()%9+1;
					}while(x==1||x==2||x==3);
				}
				else if(Eposy[i]==1){
					do{
						x=rand()%9+1;
					}while(x==7||x==8||x==9);
				}
				else x=rand()%9+1;
				Eposy[i] = movey(x,Eposy[i]);
			}
			}
			//clear the turn before
			system("cls");
			
			
		}while(gameover==0&&pass==0);
		if(pass==1)level++;
		
		pass=0;
		gameover=0;
		posx=1;
		posy=1;
	}while(level!=10);	
	return 0;
}
